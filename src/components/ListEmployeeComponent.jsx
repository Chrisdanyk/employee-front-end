import React, { Component } from 'react';
import EmployeeService from '../services/EmployeeService'
import { FaEdit, FaPlus, FaSearch, FaTrash } from 'react-icons/fa'




class ListEmployeeComponent extends Component {
    constructor(props) {
        super(props)
        this.state = { employees: [] }
        this.addEmployee = this.addEmployee.bind(this);
        this.viewEmployee = this.viewEmployee.bind(this);
        this.editEmployee = this.editEmployee.bind(this);
        this.deleteEmployee = this.deleteEmployee.bind(this);

    }
    componentDidMount() {
        EmployeeService.getEmployees().then((res) => { this.setState({ employees: res.data }) })
    }
    addEmployee() { this.props.history.push(`/add-employee/_add`) }
    viewEmployee(id) { this.props.history.push(`/view-employee/${id}`) }
    editEmployee(id) { this.props.history.push(`/add-employee/${id}`) }

    deleteEmployee(id) {
        EmployeeService.deleteEmployee(id).then(
            res => {
                this.setState(
                    {
                        employees: this.state.employees.filter(employee => employee.id !== id)
                    }
                )
            }
        )

    }


    render() {
        return (
            <div>
                <h2 className="text-center">Employees</h2>

                <div className="row">

                    <div className="col-md-10 offset-md-1 offset-md-1">
                        <table className="table table-hover table-dark table-striped table-bordered table-">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">First Name</th>
                                    <th scope="col">Last Name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Actions</th>

                                </tr>
                            </thead>
                            <tbody>
                                {this.state.employees.map(employee =>
                                    < tr key={employee.id}>
                                        <th scope="row">{employee.id}</th>
                                        <td>{employee.firstName}</td>
                                        <td>{employee.lastName}</td>
                                        <td>{employee.emailId}</td>
                                        <td>

                                            <button className="btn btn-sm btn-success" onClick={() => { this.viewEmployee(employee.id) }} ><FaSearch /></button>
                                            <button className="btn btn-sm btn-primary" onClick={() => { this.editEmployee(employee.id) }} ><FaEdit /></button>
                                            <button className="btn btn-sm btn-danger" onClick={() => { this.deleteEmployee(employee.id) }} ><FaTrash /></button>

                                        </td>
                                    </tr>

                                )
                                }
                            </tbody>
                            <tfoot>
                                <tr>

                                </tr>
                            </tfoot>
                        </table>
                        <button className="btn btn-primary float-right" style={{ marginTop: '10px' }} onClick={this.addEmployee}><FaPlus /></button>


                    </div>


                </div>


            </div>
        );
    }
}

export default ListEmployeeComponent;