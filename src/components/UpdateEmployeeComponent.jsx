import React, { Component } from 'react';
import EmployeeService from '../services/EmployeeService'
import { FaCheckDouble, FaTimes } from 'react-icons/fa'

class UpdateEmployeeComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            id: this.props.match.params.id,
            firstName: '',
            lastName: '',
            emailId: ''
        }


        this.updateEmployee = this.updateEmployee.bind(this);
    }



    changeFirstNameHandler = (event) => { this.setState({ firstName: event.target.value }) }
    changeLastNameHandler = (event) => { this.setState({ lastName: event.target.value }) }
    changeEmailIdHandler = (event) => { this.setState({ emailId: event.target.value }) }


    updateEmployee = (event) => {
        event.preventDefault();
        let employee = { firstName: this.state.firstName, lastName: this.state.lastName, emailId: this.state.emailId };
        console.log('updated employee =>' + JSON.stringify(employee));
        EmployeeService.updateEmployee(employee, this.state.id).then(res => { this.props.history.push('/employees')});
    }


    cancel() { this.props.history.push('/employees'); }

    componentDidMount() {
        EmployeeService.getEmployeeById(this.state.id).then((res) => {
            let employee = res.data;
            this.setState({
                firstName: employee.firstName, lastName: employee.lastName, emailId: employee.emailId
            })
        })
    }

    render() {
        return (
            <div>
                <div className="container">
                    <div className="row">
                        <div style={{ marginTop: '30px' }} className="card col-md-6 offset-md-3 offset-md-3">
                            <h3 className="text-center">Update Employee</h3>
                            <div className="card-body">
                                <form>
                                    <div className="form-group">
                                        <label>First Name:</label>
                                        <input placeholder="First Name:" name="firstname" className="form-control"
                                            value={this.state.firstName} onChange={this.changeFirstNameHandler} />
                                    </div>
                                    <div className="form-group">
                                        <label>Last Name:</label>
                                        <input placeholder="Last Name:" name="lastname" className="form-control"
                                            value={this.state.lastName} onChange={this.changeLastNameHandler} />
                                    </div>
                                    <div className="form-group">
                                        <label>EmailId:</label>
                                        <input placeholder="EmailId:" name="emailId" className="form-control"
                                            value={this.state.emailId} onChange={this.changeEmailIdHandler} />
                                    </div>
                                    <div className="form-group">
                                        <button className="btn btn-success" style={{ marginRight: '10px' }} onClick={this.updateEmployee}><FaCheckDouble /></button>
                                        <button className="btn btn-danger" onClick={this.cancel.bind(this)}><FaTimes /></button>

                                    </div>


                                </form>
                            </div>
                        </div>


                    </div>


                </div>

            </div>
        )
    }
}



export default UpdateEmployeeComponent;