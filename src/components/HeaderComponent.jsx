import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'

export default class HeaderComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            
        }
    }
    render() {
        return (
            <div>
                <header>
                    <nav className="navbar navbar-expand-lg navbar-expand-md  navbar-dark bg-dark">
                        <NavLink className="navbar-brand" to='/' >EMS</NavLink>
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>

                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                            {/* <ul className="navbar-nav mr-auto">
                                <li className="nav-item active">
                                    <NavLink className="nav-link" to='' >Admin <span className="sr-only">(current)</span></NavLink>
                                </li>
                            </ul> */}
                           
                        </div>
                    </nav>
                </header>
            </div>
        )
    }
}
