import React, { Component } from 'react'
import '../App.css'

export default class footerComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            
        }
    }

    render() {
        return (
            <div>
                <footer style={{ marginTop: '10px' }} className="footer">
                    <span className="text-muted">All Rights Reserved 2020 © chris dany</span>
                </footer>
            </div>
        )
    }
}
