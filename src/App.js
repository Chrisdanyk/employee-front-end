
import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'
import 'jquery/dist/jquery'
import 'bootstrap/dist/js/bootstrap'
import ListEmployeeComponent from './components/ListEmployeeComponent'
import FooterComponent from './components/FooterComponent'
import HeaderComponent from './components/HeaderComponent'
import CreateEmployeeComponent from './components/CreateEmployeeComponent'
import ViewEmployeeComponent from './components/ViewEmployeeComponent'
import UpdateEmployeeComponent from './components/UpdateEmployeeComponent'

import './App.css'
import { BrowserRouter, Route, Switch } from 'react-router-dom'


function App() {
  return (
    <div>
      <BrowserRouter>
        <HeaderComponent />
        <div className='ui-grid'>
          <Switch>
            <Route path="/" exact={true} component={ListEmployeeComponent} ></Route>
            <Route path="/employees" component={ListEmployeeComponent} ></Route>
            <Route path="/add-employee/:id" component={CreateEmployeeComponent} ></Route>
            <Route path="/view-employee/:id" component={ViewEmployeeComponent} ></Route>
            {/* <Route path="/update-employee/:id"  component={UpdateEmployeeComponent} ></Route> */}
            <ListEmployeeComponent />
          </Switch>
        </div>
        {/* <FooterComponent /> */}
      </BrowserRouter>
    </div>

  );
}

export default App;
